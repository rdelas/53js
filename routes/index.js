var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'GAME OF THRONES', lang: req.language});
});

module.exports = router;
