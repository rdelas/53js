var express = require('express'),
    episodes = require("../data/episodes"),
    bodyParser = require('body-parser'),
    router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: false}));

/* GET home page. */
router.get('/', function (req, res, next) {
    //res.render('episodes', { title: 'Liste des épisodes', episodes: episodes });
    for (var episode of episodes) {
        var formatedDuration = formatDuration(episode.duration);
        episode.formatedDuration = formatedDuration;
        episode.formatedTitle = __(episode.title);
    }
    res.send(episodes);
});

router.post('/like', function (req, res, next) {
    
    episode = getEpisodeById(req.body.episodeId);
    episode.like++;
    res.redirect("/");
});

function getEpisodeById(id) {
    for (var episode of episodes) {
        if (episode.id == id) {
            return episode;
        }
    }
    return null;
}

function formatDuration(duration) {


    var hours = Math.floor(duration / 3600);
    var minutes = Math.floor((duration - (hours * 3600)) / 60);
    var seconds = duration - (hours * 3600) - (minutes * 60);

    var result = (hours > 0) ? __n("%s HOUR", hours) + " " : "";
    result += (minutes > 0) ? __n("%s MINUTE", minutes) + " " : "";
    result += (seconds > 0) ? __n("%s SECOND", seconds) + " " : "";

    return result;
}


module.exports = router;