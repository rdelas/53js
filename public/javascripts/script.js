
var template = {"<>":"div", "class":"col-md-12 col-sm-12 col-xs-12", "html":[
    {"<>":"div",  "html":[
        {"<>" : "div", "class":"left col-md-6 col-sm-6 col-xs-6", "html":[
            {"<>":"p", "class":"episodes-bold","html":"${formatedTitle}"},
            {"<>":"p", "class":"episode-duration", "html": "${formatedDuration}" }
        ]},
        {"<>" : "div","class":"right col-md-6 col-sm-6 col-xs-6",  "html" : [
            {"<>":"form","action" : "/episodes/like", "method" :"POST", "html": [
                {"<>":"input","type":"hidden","name":"episodeId","value":"${id}","html":""},
                {"<>":"span", "class":"episodes-bold","html":"${like} "},
                {"<>":"label", "for":"like-${like}", "html":[
                    {"<>":"i", "class": "glyphicon glyphicon-thumbs-up pointer i-like"}
                ]},
                {"<>":"input","type":"submit", "id":"like-${like}", "class":"hidden", "html": [

                ]}
            ]}
        ]}

    ]},
    {"<>":"hr", "class" : "col-lg-1 col-md-1 col-sm-1 col-xs-1 col-lg-offset-5 col-md-offset-5 col-sm-offset-5 col-xs-offset-5"}
    ]};

window.onload = function (){
    getEpisodes();
};

function getEpisodes() {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/episodes?lang="+document.querySelector("#lang").value, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                document.querySelector("#episodes").innerHTML = json2html.transform(xhr.responseText,template);
            } else {
                console.error(xhr.statusText);
            }
        }
    };
    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    };
    xhr.send(null);

}
